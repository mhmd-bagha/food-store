const Header = () => {
    return (
        <>
            <div className="flex justify-center">
                {/* header image food */}
                <img src={require('../../../assets/images/pages/login/food.png')} alt="login image"
                     className="w-72 h-72"/>
            </div>
        </>
    )
}
export default Header